import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:homework3/database/calculation_history_db.dart';
import 'package:homework3/model/history.dart';

class CalculationHistory extends StatefulWidget {
  const CalculationHistory({Key? key}) : super(key: key);

  @override
  _CalculationHistoryState createState() => _CalculationHistoryState();
}

class _CalculationHistoryState extends State<CalculationHistory> {
  Future<List<History>> refreshCalculationHistory() async {
    List<History> history =
        await CalculationHistoryDatabase.instance.readAllHistory();
    return history;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('Sqflite History'),
        ),
        body: FutureBuilder(
            future: refreshCalculationHistory(),
            builder: (BuildContext context, AsyncSnapshot<List<History>> snapshot) {
              if (!snapshot.hasData) {
                return Center(child: CircularProgressIndicator());
              } else {
                return ListView.builder(
                    itemCount: snapshot.data!.length,
                    itemBuilder: (BuildContext context, int index) {
                      return ListTile(
                          leading: Icon(Icons.calculate_outlined),
                          trailing: Text(
                            snapshot.data![index].createdTime.toString(),
                            style: TextStyle(color: Colors.green, fontSize: 15),
                          ),
                          title: Text(snapshot.data![index].calculation));
                    });
              }
            })); // mainAxisAlignment: MainAxisAlignment.center,
  }
}
