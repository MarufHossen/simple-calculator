import 'package:flutter/material.dart';
import 'package:homework3/calculator/CalculationController.dart';
import 'package:homework3/ui/widgets/calculator_button.dart';

class CalculatorPage extends StatefulWidget {
  const CalculatorPage({Key? key}) : super(key: key);

  @override
  State<CalculatorPage> createState() => _CalculatorPageState();
}

class _CalculatorPageState extends State<CalculatorPage> {
  String _history = '';
  String _expression = '';

  void numClick(String text) {
    setState(() => _expression += text);
  }

  void allClear(String text) {
    setState(() {
      _history = '';
      _expression = '';
    });
  }

  void clear(String text) {
    setState(() {
      _expression = '';
    });
  }

  void evaluate(String text) {
    String result = CalculationController.getCalculationResult(_expression);
    setState(() {
      _history = _expression;
      _expression = result;
    });
    String calculationData = _history + " = " + _expression;
    CalculationController.addCalculation(calculationData);
  }

  

  @override
  Widget build(BuildContext context) {
    return Container(
      // padding: EdgeInsets.all(12),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.end,
        children: <Widget>[
          Container(
            child: Padding(
                padding: const EdgeInsets.only(right: 12),
                child: Text(
                  _history,
                  style: TextStyle(
                    fontSize: 24,
                    color: Color(0xFF545F61),
                  ),
                )),
            alignment: Alignment(1.0, 1.0),
          ),
          Container(
            child: Padding(
              padding: const EdgeInsets.all(12),
              child: Text(
                _expression,
                style: TextStyle(
                  fontSize: 48,
                  color: Colors.white,
                ),
              ),
            ),
            alignment: Alignment(1.0, 1.0),
          ),
          SizedBox(height: 40),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: <Widget>[
              CalculatorButton(
                text: 'AC',
                fillColor: 0xFF6C807F,
                textSize: 20,
                callback: allClear,
              ),
              CalculatorButton(
                text: 'C',
                fillColor: 0xFF6C807F,
                callback: clear,
              ),
              CalculatorButton(
                text: '^',
                fillColor: 0xFFFFFFFF,
                textColor: 0xFF65BDAC,
                callback: numClick,
              ),
              CalculatorButton(
                text: '/',
                fillColor: 0xFFFFFFFF,
                textColor: 0xFF65BDAC,
                callback: numClick,
              ),
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: <Widget>[
              CalculatorButton(
                text: '7',
                callback: numClick,
              ),
              CalculatorButton(
                text: '8',
                callback: numClick,
              ),
              CalculatorButton(
                text: '9',
                callback: numClick,
              ),
              CalculatorButton(
                text: '*',
                fillColor: 0xFFFFFFFF,
                textColor: 0xFF65BDAC,
                textSize: 24,
                callback: numClick,
              ),
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: <Widget>[
              CalculatorButton(
                text: '4',
                callback: numClick,
              ),
              CalculatorButton(
                text: '5',
                callback: numClick,
              ),
              CalculatorButton(
                text: '6',
                callback: numClick,
              ),
              CalculatorButton(
                text: '-',
                fillColor: 0xFFFFFFFF,
                textColor: 0xFF65BDAC,
                textSize: 38,
                callback: numClick,
              ),
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: <Widget>[
              CalculatorButton(
                text: '1',
                callback: numClick,
              ),
              CalculatorButton(
                text: '2',
                callback: numClick,
              ),
              CalculatorButton(
                text: '3',
                callback: numClick,
              ),
              CalculatorButton(
                text: '+',
                fillColor: 0xFFFFFFFF,
                textColor: 0xFF65BDAC,
                textSize: 30,
                callback: numClick,
              ),
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: <Widget>[
              CalculatorButton(
                text: '.',
                callback: numClick,
              ),
              CalculatorButton(
                text: '0',
                callback: numClick,
              ),
              CalculatorButton(
                text: '00',
                callback: numClick,
                textSize: 26,
              ),
              CalculatorButton(
                text: '=',
                fillColor: 0xFFFFFFFF,
                textColor: 0xFF65BDAC,
                callback: evaluate,
              ),
            ],
          )
        ],
      ),
    );
  }
}
