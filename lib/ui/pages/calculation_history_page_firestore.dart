import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:homework3/database/calculation_history_firestore.dart';

class CalculationHistoryFirestore extends StatefulWidget {
  const CalculationHistoryFirestore({Key? key}) : super(key: key);

  @override
  _CalculationHistoryFirestoreState createState() => _CalculationHistoryFirestoreState();
}

class _CalculationHistoryFirestoreState extends State<CalculationHistoryFirestore> {
  Future<QuerySnapshot> refreshCalculationHistoryFirestore() {
    // List<History> history = ;
    return FirestoreService().getAllHistory();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('History Firestore'),
        ),
        body: FutureBuilder(
            future: refreshCalculationHistoryFirestore(),
            builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
              if (!snapshot.hasData) {
                return Center(child: CircularProgressIndicator());
              } else {
                return ListView.builder(
                    itemCount: snapshot.data!.docs.length,
                    itemBuilder: (BuildContext context, int index) {
                      return ListTile(
                          leading: Icon(Icons.calculate),
                          title: Text(snapshot.data!.docs[index]["calculation"]));
                    });
              }
            })); // mainAxisAlignment: MainAxisAlignment.center,
  }
}
