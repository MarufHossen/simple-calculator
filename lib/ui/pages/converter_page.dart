import 'package:flutter/material.dart';
import 'package:homework3/converters/Converter.dart';
import 'package:homework3/converters/KmToMileConverter.dart';
import 'package:homework3/converters/MileToKmConverter.dart';

class ConverterPage extends StatefulWidget {
  const ConverterPage({Key? key}) : super(key: key);

  @override
  _ConverterPageState createState() => _ConverterPageState();
}

class _ConverterPageState extends State<ConverterPage> {
  final kmInputController = TextEditingController();
  final mileInputController = TextEditingController();

  final Converter kmToMileConverter = KmToMileConverter();
  final Converter mileToKmConverter = MileToKmConverter();

  double km = 0.0, mile = 0.0;
  void getConvertedKm(double number){

    km = mileToKmConverter.convert(number);

    setState(() {
      kmInputController.text = km.toString();
    });
  }

  void getConvertedMile(double number){
    mile = kmToMileConverter.convert(number);
    setState(() {
      mileInputController.text = mile.toString();
    });
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('Converter'),
        ),
        body: Padding(
            padding: const EdgeInsets.all(10),
            child: Column(
                // mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  TextFormField(
                    controller: kmInputController,
                    // initialValue:,
                    keyboardType: TextInputType.number,
                    decoration:
                        const InputDecoration(hintText: "Enter KM", labelText: 'KM'),
                    onChanged: (String num){
                      mileInputController.text = kmToMileConverter.convert(double.parse(num)).toString();
                    },
                  ), 
                  SizedBox(
                    height: 20,
                  ),
                  Text(
                    'To',
                    style: TextStyle(fontSize: 25),
                  ),
                  TextFormField(
                    controller: mileInputController,
                    keyboardType: TextInputType.number,
                    decoration:
                        const InputDecoration(hintText: "Enter a Mile", labelText: 'Mile'),
                    onChanged: (String num){
                      kmInputController.text = mileToKmConverter.convert(double.parse(num)).toString();
                    },
                  ),

                  SizedBox(
                    height: 20,
                  ),

                ])));
  }
}
