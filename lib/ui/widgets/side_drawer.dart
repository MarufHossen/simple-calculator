import 'package:flutter/material.dart';
import 'package:homework3/ui/pages/calculation_history_page.dart';
import 'package:homework3/ui/pages/calculation_history_page_firestore.dart';
import 'package:homework3/ui/pages/converter_page.dart';

class SideDrawer extends StatelessWidget {
  const SideDrawer({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: ListView(
        // Important: Remove any padding from the ListView.
        padding: EdgeInsets.zero,
        children: [
          const DrawerHeader(
            decoration: BoxDecoration(
              color: Colors.blueGrey
            ),
            child: Center(
                child: Text('Calculator',
                    style: TextStyle(color: Colors.white, fontSize: 25))),
          ),
          ListTile(
            title: const Text('Calculator'),
            onTap: () {
              // Update the state of the app
              // ...
              // Then close the drawer
              Navigator.pop(context);
            },
          ),
          ListTile(
            title: const Text('Converter'),
            onTap: () {
              // Update the state of the app
              // ...
              // Then close the drawer
              Navigator.pop(context);
              Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => ConverterPage()),
                  );
            },
          ),
          ListTile(
            title: const Text('History - Sqflite'),
            onTap: () {

              // Then close the drawer
              Navigator.pop(context);
              // Update the state of the app
              // ...
              Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => CalculationHistory()),
                  );
            },
          ),
          ListTile(
            title: const Text('History - Firestore'),
            onTap: () {
              // Update the state of the app
              // ...
              // Then close the drawer
              Navigator.pop(context);
              Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => CalculationHistoryFirestore()),
                  );
            },
          ),
        ],
      ),
    );
  }
}
