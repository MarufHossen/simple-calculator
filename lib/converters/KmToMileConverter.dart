import 'package:homework3/converters/Converter.dart';

class KmToMileConverter implements Converter{
  @override
  double convert(double number) {
    return number*0.621371;
  }

}