import 'package:homework3/converters/Converter.dart';

class MileToKmConverter implements Converter{

  @override
  double convert(double number) {
    return number*1.60934;
  }

}