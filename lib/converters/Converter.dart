abstract class Converter{
  double convert(double number);
}