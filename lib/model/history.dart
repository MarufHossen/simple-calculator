final String tableHistory = 'history';

class HistoryFields {
  static final List<String> values = [
    /// Add all fields
    id, calculation, time
  ];

  static final String id = '_id';
  static final String calculation = 'calculation';
  static final String time = 'time';
}

class History {
  final int? id;
  final String calculation;
  final DateTime createdTime;

  const History({
    this.id,
    required this.calculation,
    required this.createdTime,
  });

  History copy({
    int? id,
    String? calculation,
    DateTime? createdTime,
  }) =>
      History(
        id: id ?? this.id,
        calculation: calculation ?? this.calculation,
        createdTime: createdTime ?? this.createdTime,
      );

  static History fromJson(Map<String, Object?> json) => History(
    id: json[HistoryFields.id] as int?,
    calculation: json[HistoryFields.calculation] as String,
    createdTime: DateTime.parse(json[HistoryFields.time] as String),
  );

  Map<String, Object?> toJson() => {
    HistoryFields.id: id,
    HistoryFields.calculation: calculation,
    HistoryFields.time: createdTime.toIso8601String(),
  };
}