import 'package:homework3/database/calculation_history_db.dart';
import 'package:homework3/database/calculation_history_firestore.dart';
import 'package:homework3/model/history.dart';
import 'package:math_expressions/math_expressions.dart';

class CalculationController{
  static String getCalculationResult(String expression){
    Parser p = Parser();
    Expression exp = p.parse(expression);
    ContextModel cm = ContextModel();
    String result = exp.evaluate(EvaluationType.REAL, cm).toString();
    return result;
  }

  static Future addCalculation(String calculation) async {
    final calculationHistory = History(
      calculation: calculation,
      createdTime: DateTime.now(),
    );

    await CalculationHistoryDatabase.instance.create(calculationHistory);
    await FirestoreService().addNewCalculation(calculation);
  }
}