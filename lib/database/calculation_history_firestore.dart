import 'package:cloud_firestore/cloud_firestore.dart';

class FirestoreService {
  FirebaseFirestore firestore = FirebaseFirestore.instance;

  CollectionReference history =
      FirebaseFirestore.instance.collection('history');

  Future<void> addNewCalculation(String calculation) {
    // Call the user's CollectionReference to add a new user
    return history
        .add({
          'calculation': calculation,
        })
        .then((value) => print("Calcualtion Added"))
        .catchError((error) => print("Failed to add CALCULATION: $error"));
  }

  Future<QuerySnapshot> getAllHistory() {
    return history.get();
  }
}
